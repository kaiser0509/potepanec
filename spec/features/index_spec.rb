require 'rails_helper'

RSpec.feature "Index", type: :feature do
  given!(:taxon) { create(:taxon) }
  given!(:new_product) { create(:product_with_option_types, taxons: [taxon]) }
  background do
    visit potepan_path
  end

  scenario "index page have correct elements" do
    expect(page).to have_title "BIGBAG Store"
    within("div.featuredProducts") do
      expect(page).to have_link new_product.name, href: potepan_product_path(new_product.id)
      expect(page).to have_content new_product.display_price
    end
  end

  scenario "click new_product's link and render show page" do
    click_link new_product.name
    expect(page).to have_current_path potepan_product_path(new_product.id)
  end
end
