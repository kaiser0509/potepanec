require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  given!(:taxonomy) { create(:taxonomy) }
  given!(:taxon1) { create(:taxon, name: "bags", parent: taxonomy.root) }
  given!(:taxon2) { create(:taxon, name: "mugs", parent: taxonomy.root) }
  given!(:product1) { create(:product, taxons: [taxon1]) }
  background do
    visit potepan_category_path(taxon1.id)
  end

  scenario "category's show page have correct elements" do
    expect(page).to have_title "#{taxon1.name} - BIGBAG Store"
    within("div.page-title") do
      expect(page).to have_content taxon1.name
    end
    within("ul.side-nav") do
      expect(page).to have_content taxonomy.name
      expect(page).to have_link "#{taxon1.name}(#{taxon1.products.size})", href: potepan_category_path(taxon1.id)
      expect(page).to have_link "#{taxon2.name}(#{taxon2.products.size})", href: potepan_category_path(taxon2.id)
    end
    within("div.productBox") do
      expect(page).to have_link product1.name, href: potepan_product_path(product1.id)
      expect(page).to have_content product1.display_price
    end
  end

  scenario "click category's link and render show page" do
    click_link "#{taxon2.name}(#{taxon2.products.size})"
    expect(page).to have_current_path potepan_category_path(taxon2.id)
    expect(page).not_to have_content product1.name
    expect(page).not_to have_content product1.display_price
  end

  scenario "click product's link and render show page" do
    click_link product1.name
    expect(page).to have_current_path potepan_product_path(product1.id)
  end
end
