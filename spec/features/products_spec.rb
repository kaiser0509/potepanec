require 'rails_helper'

RSpec.feature "Products", type: :feature do
  given!(:taxon) { create(:taxon) }
  given!(:product) { create(:product_with_option_types, taxons: [taxon]) }
  given!(:related_product) { create(:product, name: "related_product", price: 20.00, taxons: [taxon]) }
  background do
    product.option_types.each do |option_type|
      option_type.option_values.create(attributes_for(:option_value))
    end
    visit potepan_product_path(product.id)
  end

  scenario "user visit product show page" do
    expect(page).to have_title "#{product.name} - BIGBAG Store"

    within("ol.breadcrumb") do
      expect(page).to have_content "Home"
      expect(page).to have_content product.name
    end

    within("div.media-body") do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      expect(page).to have_content product.description
      within("select#id") do
        product.option_types.each do |option_type|
          option_type.option_values.each do |option_value|
            expect(page).to have_content option_value.presentation
          end
        end
      end
    end
    expect(page).to have_link 'カートへ入れる'

    within("div.productsContent") do
      expect(page).to have_link related_product.name, href: potepan_product_path(related_product.id)
      expect(page).to have_content related_product.display_price
    end
  end

  scenario "click related product's link and render show page" do
    click_link related_product.name
    expect(page).to have_current_path potepan_product_path(related_product.id)
  end

  scenario "click back link and render category page" do
    click_link "一覧ページへ戻る"
    expect(page).to have_current_path potepan_category_path(taxon.id)
  end
end
