require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "GET #show" do
    let!(:taxon) { create(:taxon) }
    let!(:taxonomy) { taxon.taxonomy }
    let!(:product) { create(:product, taxons: [taxon]) }

    before do
      get :show, params: { id: taxon.id }
    end

    it "responds successfully" do
      expect(response).to be_successful
    end

    it "render show template" do
      expect(response).to render_template(:show)
    end

    it "get taxon correctly" do
      expect(assigns(:taxon)).to eq taxon
    end

    it "get taxonomies correctly" do
      expect(assigns(:taxonomies)).to match_array taxonomy
    end

    it "get product correctly" do
      expect(assigns(:products)).to include product
    end
  end
end
