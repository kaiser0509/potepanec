require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  let!(:taxon) { create(:taxon) }
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

  describe "GET #show" do
    before do
      get :show, params: { id: product.id }
    end

    it "have product correctly" do
      expect(assigns(:product)).to eq product
    end

    it "responds successfully" do
      expect(response).to be_successful
    end

    it "render correctly" do
      expect(response).to render_template(:show)
    end

    it "have taxon correctly " do
      expect(assigns(:product).taxons).to include taxon
    end

    it "have correct number's related_products" do
      expect(assigns(:related_products).size).to eq 4
    end

    it "not have self product" do
      expect(assigns(:related_products)).not_to include :product
    end

    it "not have duplication" do
      expect(assigns(:related_products).size).to eq assigns(:related_products).uniq.size
    end
  end
end
