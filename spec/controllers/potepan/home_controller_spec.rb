require 'rails_helper'

RSpec.describe Potepan::HomeController, type: :controller do
  describe "GET #index" do
    let!(:first_product) { create(:product, available_on: 1.day.ago) }
    let!(:after_products) { create_list(:product, 8, available_on: 2.day.ago) }
    let!(:last_product) { create(:product, available_on: 3.day.ago) }
    let!(:non_product) { create(:product, available_on: 4.day.ago) }

    before do
      get :index
    end

    it "responds successfully" do
      expect(response).to be_successful
    end

    it "render index template" do
      expect(response).to render_template(:index)
    end

    it "have first_product correctly" do
      expect(assigns(:new_products).first).to eq first_product
    end

    it "have last_product correctly" do
      expect(assigns(:new_products).last).to eq last_product
    end

    it "not have last_product correctly" do
      expect(assigns(:new_products)).not_to include non_product
    end

    it "have correct number products" do
      expect(assigns(:new_products).size).to eq 10
    end
  end
end
