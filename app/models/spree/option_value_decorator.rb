Spree::OptionValue.class_eval do
  def products
    ids = Spree::Product.with_option_value(option_type_id, name).pluck(:id)
    Spree::Product.where(id: ids)
  end

  def count_products
    products.count
  end
end
