Spree::Product.class_eval do
  scope :related_products, -> (product) do
    in_taxons(product.taxons).distinct.where.not(id: product.id)
  end
  scope :searched_by, -> (keyword) do
    where('name LIKE :keyword OR description LIKE :keyword',
          keyword: "%#{sanitize_sql_like(keyword)}%")
  end
  scope :with_option, -> (option_type, option_value) do
    ids = Spree::Product.with_option_value(option_type, option_value).pluck(:id)
    where(id: ids)
  end

  class << self
    def sorted_by(sort_type)
      case sort_type
      when "date_desc"
        order(available_on: :desc)
      when "price_asc"
        ascend_by_master_price
      when "price_desc"
        descend_by_master_price
      when "date_asc"
        order(available_on: :asc)
      else
        order(available_on: :desc)
      end
    end
  end

end
