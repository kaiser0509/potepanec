class Potepan::ProductsController < ApplicationController
  include SortFilter
  MAX_NUMBER_OF_RELATED_PRODUCTS = 4

  def index
    sort_options
    @taxonomies = Spree::Taxonomy.includes(root: :children).all
    @products = Spree::Product.includes(master: [:default_price, :images])
                .with_option(params[:option_type],params[:option_value])
                .sorted_by(params[:sort])
    @colors = Spree::OptionType.find_by(presentation: "Color").option_values
    @sizes = Spree::OptionType.find_by(presentation: "Size").option_values
  end

  def search
    sort_options
    @taxonomies = Spree::Taxonomy.includes(root: :children).all
    @products = Spree::Product.includes(master: [:default_price, :images])
                .searched_by(params[:keyword])
                .sorted_by(params[:sort])
    @colors = Spree::OptionType.find_by(presentation: "Color").option_values
    @sizes = Spree::OptionType.find_by(presentation: "Size").option_values
    render :index
  end

  def show
    @product = Spree::Product.includes(option_types: [:option_values]).find(params[:id])
    @related_products = Spree::Product.related_products(@product).limit(MAX_NUMBER_OF_RELATED_PRODUCTS)
  end
end
