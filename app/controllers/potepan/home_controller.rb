class Potepan::HomeController < ApplicationController
  NEW_PRODUCTS_MAX_NUMBER = 10
  def index
    @new_products = Spree::Product.includes(master: [:default_price, :images]).order(available_on: "DESC").
      limit(NEW_PRODUCTS_MAX_NUMBER)
  end
end
