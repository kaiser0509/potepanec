class Potepan::CategoriesController < ApplicationController
  include SortFilter

  def show
    sort_options
    @taxonomies = Spree::Taxonomy.includes(root: :children).all
    @taxon = Spree::Taxon.includes(:products).find(params[:id])
    @products = Spree::Product.in_taxons(@taxon).includes(master: [:default_price, :images])
                .sorted_by(params[:sort])
    @colors = Spree::OptionType.find_by(presentation: "Color").option_values
    @sizes = Spree::OptionType.find_by(presentation: "Size").option_values
  end
end
