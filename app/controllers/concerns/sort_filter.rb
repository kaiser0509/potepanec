module SortFilter
  def sort_options
    @sort_options = [
                      ["新着順", "date_desc"],
                      ["価格の安い順", "price_asc"],
                      ["価格の高い順", "price_desc"],
                      ["古い順", "date_asc"],
                    ]
  end
end
